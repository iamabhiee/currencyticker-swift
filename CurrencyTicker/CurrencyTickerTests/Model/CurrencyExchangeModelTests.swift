//
//  CurrencyExchangeModelTests.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 06/09/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import XCTest

@testable import CurrencyTicker

class CurrencyExchangeModelTests: XCTestCase {
    
    var sut : CurrencyExchange? = nil
    let date = NSDate()
    let rate : NSNumber = 1.0
    let prevRate : NSNumber = 2.0

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let currencyManager = CurrencyModelTests()
        sut = CurrencyExchange(currency: currencyManager.firstCurrencyModel(), baseCurrency: currencyManager.secondCurrencyModel(), rate: rate, date: date)
        sut?.previousConversationRate = prevRate
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        sut = nil
    }

    func testInit_ShouldNotBeNil() {
        XCTAssertNotNil(sut, "CurrencyExchange should not be nil")
    }
    
    func testInitAllProperties_ShouldNotBeNil() {
        XCTAssertNotNil(sut?.baseCurrency, "Base Currency should not be nil")
        XCTAssertNotNil(sut?.currency, "Currency should not be nil")
        XCTAssertNotNil(sut?.date, "Date should not be nil")
        XCTAssertNotNil(sut?.conversationRate, "Currency should not be nil")
    }
    
    func testInitAllPropertiesValue_ShouldBesame() {
        let currencyManager = CurrencyModelTests()
        
        XCTAssertEqual(sut?.currency, currencyManager.firstCurrencyModel(), "Currency should be same")
        XCTAssertEqual(sut?.baseCurrency, currencyManager.secondCurrencyModel(), "Base Currency should be same")
        XCTAssertEqual(sut?.date, date, "Date should be same")
        XCTAssertEqual(sut?.conversationRate, rate, "Rate should be same")
    }
    
    func testPreviousRate_ShouldBeSame() {
        XCTAssertNotNil(sut?.previousConversationRate, "Prev Rate should not be nil")
        XCTAssertEqual(sut?.previousConversationRate, prevRate, "Rate should be same")
    }
    
    func testDifference_ShouldBeCorrect() {
        XCTAssertEqual(sut?.differenceValue, -1.0, "Rate should be correct")
    }
    
    func testTitleString_ShouldContainCodeAndValue() {
        let currencyManager = CurrencyModelTests()
        let titleString = sut?.titleString
        
        let baseCurrency = currencyManager.firstCurrencyModel()
        let firstCurrency = currencyManager.secondCurrencyModel()
        let containsBaseCurrency = titleString?.containsString(baseCurrency.code)
        let containsOtherCurrency = titleString?.containsString(firstCurrency.code)
        XCTAssertTrue(containsBaseCurrency!, "Should contain base currency title")
        XCTAssertTrue(containsOtherCurrency!, "Should contain other currency title")
    }
}
