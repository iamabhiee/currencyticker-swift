//
//  ExchangeDetailsViewController.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 31/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import UIKit
import CoreGraphics
import UUChartView


class ExchangeDetailsViewController: UIViewController {
    
    @IBOutlet var myGraph : UIView!
    
    @IBOutlet var lblCurrentValue : UILabel!
    @IBOutlet var lblCurrencyCode : UILabel!
    
    @IBOutlet var priceChangeContainerView : CurrencyPriceChangeView!
    
    @IBOutlet var lblWeeklyHighValue : UILabel!
    @IBOutlet var lblWeeklyLowValue : UILabel!
    
    var chartView : UUChart!
    
    var pastData : [CurrencyExchange] = []
    var xAxisData : [String] = []
    var yAxisData : [Float] = []
    
    var selectedCurrencyExchange : CurrencyExchange? = nil
    
    var chartRange : CGRange {
        get {
            if let numMax = yAxisData.maxElement(), let numMin = yAxisData.minElement() {
                return CGRange(max: CGFloat(numMax), min: CGFloat(numMin))
            } else {
                return CGRange(max: 0, min: 0)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //init chart
        let screenSize = UIScreen.mainScreen().bounds
        let height = myGraph.frame.height - 20
        let width = screenSize.width
        let rect = CGRect(x: 0, y: 0, width: width, height: height)
        chartView = UUChart(frame: rect, dataSource: self, style: .Line)
        
        //Fetch data
        if let selectedExchange = selectedCurrencyExchange {
            //Title
            title = selectedExchange.titleString
            lblCurrencyCode.text = selectedExchange.currency.code;
            
            //Show Current Values
            priceChangeContainerView.configureWithExchange(selectedExchange)
            
            fetchData(selectedExchange)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func fetchData(currencyExchange : CurrencyExchange) {
        showProgressHud()
        
        ExchangeDataManager.fetchPastWeekExchangeData(currencyExchange.currency, baseCurrency: currencyExchange.baseCurrency, success: { (result) in
                self.hideProgressHud()
            
            if let exchangeData = result {
                self.configureGraph(exchangeData)
            }
            }) { (error) in
                self.handleError(error)
        }
    }
    
    func configureGraph(exchangeData : [CurrencyExchange]) {
        //Store data
        pastData = exchangeData
        
        //Set Axes Data
        for exchange in self.pastData {
            yAxisData.append(exchange.conversationRate.floatValue)
            xAxisData.append((exchange.date?.shortStringValue)!)
        }
        
        //Set weekly high and low data
        let range = chartRange
        lblWeeklyLowValue.text = String(format: "%.4f", range.min)
        lblWeeklyHighValue.text = String(format: "%.4f", range.max)
        
        chartView.showInView(self.myGraph)
    }
}

extension ExchangeDetailsViewController : UUChartDataSource {
    
    //MARK : required
    func chartConfigAxisXLabel(chart: UUChart!) -> [AnyObject]! {
        return xAxisData
    }
    
    func chartConfigAxisYValue(chart: UUChart!) -> [AnyObject]! {
        return [yAxisData]
    }
    
    //MARK : Optional
    func chartConfigColors(chart: UUChart!) -> [AnyObject]! {
        return [UIColor.ct_blueColor()];
    }
    
    func chartRange(chart: UUChart!) -> CGRange {
        return chartRange
    }
}
