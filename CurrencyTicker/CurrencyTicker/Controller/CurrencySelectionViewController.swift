//
//  DefaultCurrencySelectionViewController.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 29/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import UIKit

protocol CurrencySelectionControllerDelegate: class {
    func didSelectCurrencies(currencies : [Currency], sender: CurrencySelectionViewController)
}

class CurrencySelectionViewController: UIViewController {
    
    enum Mode {
        case DefaultCurrencySelection, FavoriteCurrencySelection
    }

    
    @IBOutlet var tableView : UITableView!
    
    lazy var currencies : [Currency] = []
    lazy var selectedCurrencies : [Currency] = []
    lazy var mode : Mode = .DefaultCurrencySelection
    
    weak var delegate : CurrencySelectionControllerDelegate?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.registerNib(CurrencyListTableViewCell.NibObject(), forCellReuseIdentifier: CurrencyListTableViewCell.identifier)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - IBAction
    
    @IBAction func close(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func done(sender: UIButton) {
        dismissViewControllerAnimated(true) { 
            self.delegate?.didSelectCurrencies(self.selectedCurrencies, sender: self)
        }
    }
    
    func isCurrencySelected(currency : Currency) -> Bool {
        return selectedCurrencies.contains(currency)
    }
}

extension CurrencySelectionViewController : UITableViewDataSource {
    // MARK: - UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CurrencyDataManager.sharedInstance.allCurrencies.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CurrencyListTableViewCell.identifier, forIndexPath: indexPath) as! CurrencyListTableViewCell
        let currency = currencies[indexPath.row]
        let isSelected = isCurrencySelected(currency)
        cell.configureWithCurrency(currency, isFavorite : isSelected)
        return cell
    }
}

extension CurrencySelectionViewController : UITableViewDelegate {
    // MARK: - UITableViewDelegate
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let currency = currencies[indexPath.row]
        
        if mode == .DefaultCurrencySelection {
            selectedCurrencies = [currency]
        } else {
            if selectedCurrencies.contains(currency) {
                selectedCurrencies.removeObject(currency)
            } else {
                selectedCurrencies.append(currency)
            }
        }
        
        tableView.reloadData()
    }
}