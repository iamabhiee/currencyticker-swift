//
//  ExchangeDataManager.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 30/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Foundation

typealias ExchangeCompletionBlock = (result: [CurrencyExchange]?, error: NSError?) -> Void

class ExchangeDataManager: NSObject {
    
    class func fetchLatestExchangeDataWithDifference(currencies : [Currency], baseCurrency : Currency, completion : ExchangeCompletionBlock) {
        let errorCompletionHandler:(NSError?)->Void = { error in
            completion(result: nil, error: error)
        }
        
        let successCompletionHandler:([CurrencyExchange])->Void = { data in
            completion(result: data, error: nil)
        }
        
        let today = NSDate()
        fetchExchangeData(currencies, baseCurrency: baseCurrency, date: today, success: { data, date in
            if let firstData = data, let firstDate = date {
                let previousDate = firstDate.dateAfterAddingDays(-1)
                
                fetchExchangeData(currencies, baseCurrency: baseCurrency, date: previousDate, success: { data, date in
                    
                    if let secondData = data {
                        for index in 0...firstData.count - 1 {
                            let firstCurrencyExchangeData = firstData[index]
                            let secondCurrencyExchangeData = secondData[index]
                            
                            firstCurrencyExchangeData.previousConversationRate = secondCurrencyExchangeData.conversationRate
                        }
                    }
                    
                    successCompletionHandler(firstData)
                    
                }, failure: errorCompletionHandler)
            } else {
                //TODO : No data error
                errorCompletionHandler(nil)
            }
        }, failure: errorCompletionHandler)
    }
    
    class func fetchPastWeekExchangeData(currency : Currency, baseCurrency : Currency, success : (result: [CurrencyExchange]?) -> Void, failure : (error : NSError?) -> Void) {
        let date = NSDate()
        let existingData : [CurrencyExchange] = []
        let days = 7
        
        fetchRecursiveDataForCurrency(currency, baseCurrency: baseCurrency, days: days, date: date, existingData: existingData, success: success, failure: failure)
    }
    
    //MARK : API Calls
    
    class func fetchExchangeData(currencies : [Currency], baseCurrency : Currency, date : NSDate, success : (result: [CurrencyExchange]?, date : NSDate?) -> Void, failure : (error : NSError?) -> Void) {
        let dateString = date.stringValue
        let currenciesString = currencies.map{$0.code}.joinWithSeparator(",")
        let apiEndpoint = "\(dateString)?symbols=\(currenciesString)&base=\(baseCurrency.code)"
        
        NetowrkingHelper.sharedInstance.get(apiEndpoint, parameters: [:]) {data , error in
            if error != nil {
                failure(error: error)
            } else if let responseObject = data {
                let rates = responseObject["rates"] as! [String : NSNumber]
                let dateString = responseObject["date"] as! String
                
                var exchangeData : [CurrencyExchange] = []
                if let exchangeDate = NSDate.dateFromString(dateString) {
                    for currency in currencies {
                        let rate = rates[currency.code];
                        let exchange = CurrencyExchange(currency: currency, baseCurrency: baseCurrency, rate: rate!, date: exchangeDate)
                        exchangeData.append(exchange)
                    }
                    success(result: exchangeData, date: exchangeDate)
                } else {
                    success(result: exchangeData, date: nil)
                }
            } else {
                failure(error: nil)
            }
        }
    }
    
    class func fetchRecursiveDataForCurrency(currency : Currency, baseCurrency : Currency, days : Int, date : NSDate, existingData : [CurrencyExchange], success : (result: [CurrencyExchange]?) -> Void, failure : (error : NSError?) -> Void) {
        
        var existingExchangeData = existingData
        fetchExchangeData([currency], baseCurrency: baseCurrency, date: date, success: { (result, date) in
            if let resultData = result {
                existingExchangeData += resultData
                
                let remainingDays = days - 1
                if remainingDays == 0 {
                    existingExchangeData = existingExchangeData.reverse()
                    success(result: existingExchangeData);
                } else {
                    if let previousDay = date?.dateAfterAddingDays(-1) {
                        fetchRecursiveDataForCurrency(currency, baseCurrency: baseCurrency, days: remainingDays, date: previousDay, existingData: existingExchangeData, success: success, failure: failure)
                    } else {
                        //TODO : Custom error code
                        failure(error: nil)
                    }
                }
            }
            }, failure: failure)
    }
}
