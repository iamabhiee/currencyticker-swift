//
//  CurrencyDataManager.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 18/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Foundation

class CurrencyDataManager: NSObject {
    static let sharedInstance = CurrencyDataManager()
    
    // TODO : Add Data Store as dependecy
    
    //Default Currency
    var defaultCurrency : Currency {
        didSet {
            if oldValue != defaultCurrency {
                CurrencyDataStore.sharedInstance.saveDefaultCurrency(defaultCurrency)
            }
        }
    }
    
    var favCurrencies : [Currency] {
        didSet {
            if oldValue != favCurrencies {
                CurrencyDataStore.sharedInstance.saveFavoriteCurrencies(favCurrencies)
            }
        }
    }
    
    //All Currencies
    lazy var allCurrencies = CurrencyDataStore.sharedInstance.getAllCurrencies()
    
    override init() {
        defaultCurrency = CurrencyDataManager.getDefaultCurrency()
        favCurrencies = CurrencyDataStore.sharedInstance.getFavoriteCurrencies()
        
        super.init()
    }
    
    static func getDefaultCurrency() -> Currency {
        if let currency = CurrencyDataStore.sharedInstance.getDefaultCurrency() {
            return currency
        } else {
            let defaultCurrency = CurrencyDataStore.sharedInstance.getAllCurrencies().first!
            return defaultCurrency
        }
    }
    
    func currencyAtIndex(index : Int) -> Currency {
        return allCurrencies[index]
    }
    
    func toggleFavoriteForCurrencyAtIndex(index : Int) {
        let currency = currencyAtIndex(index)
        
        if favCurrencies.contains(currency) {
            favCurrencies.removeObject(currency)
        } else {
            favCurrencies.append(currency)
        }
    }
    
    func isCurrencySelected(currency : Currency) -> Bool {
        return favCurrencies.contains(currency)
    }
    
    func currencyForCode(code : String) -> Currency {
        let matchingCurrencies = self.allCurrencies.filter({$0.code == code});
        return matchingCurrencies.first!
    }
}
