//
//  CurrencyDataStore.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 18/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Foundation

class CurrencyDataStore: NSObject {
    static let sharedInstance = CurrencyDataStore()
    
    static let defaultCurrencyKey = "DefaultCurrency"
    static let favoriteCurrenciesKey = "FavoriteCurrency"
    
    lazy var userDefaults = NSUserDefaults.standardUserDefaults()
    
    // MARK : Default
    
    func getDefaultCurrency() -> Currency? {
        
        var currency : Currency? = nil
        if let savedCurrency = userDefaults.objectForKey(CurrencyDataStore.defaultCurrencyKey) as? NSData {
            currency = NSKeyedUnarchiver.unarchiveObjectWithData(savedCurrency) as? Currency
        }
        
        return currency
    }
    
    func saveDefaultCurrency(currency : Currency) {
        let savedData = NSKeyedArchiver.archivedDataWithRootObject(currency)
        userDefaults.setObject(savedData, forKey: CurrencyDataStore.defaultCurrencyKey)
    }
    
    // MARK: - All Currencies
    
    func getAllCurrencies() -> [Currency] {
        var currencies : [Currency] = []
        if let filePath = NSBundle.mainBundle().pathForResource("currencies", ofType: "json") {
            if let data = NSData(contentsOfFile: filePath) {
                do {
                    let jsonCurrency = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers) as! [[String]]
                    
                    for currency in jsonCurrency {
                        if let currencyObj = Currency(arrayData: currency) {
                            currencies.append(currencyObj)
                        }
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        }
        return currencies
    }
    
    // MARK: - Favorite
    
    func getFavoriteCurrencies() -> [Currency] {
        let storedFavoriteCurrencies = userDefaults.objectForKey(CurrencyDataStore.favoriteCurrenciesKey) as? NSData
        
        var currencies : [Currency] = []
        if let storedFavoriteCurrencies = storedFavoriteCurrencies {
            let placesArray = NSKeyedUnarchiver.unarchiveObjectWithData(storedFavoriteCurrencies) as! [Currency]
            currencies = currencies + placesArray
        }
        return currencies
    }
    
    func saveFavoriteCurrencies(currencies : [Currency]) {
        if currencies.count > 0 {
            let currenciesData = NSKeyedArchiver.archivedDataWithRootObject(currencies)
            userDefaults.setObject(currenciesData, forKey: CurrencyDataStore.favoriteCurrenciesKey)
        } else {
            userDefaults.removeObjectForKey(CurrencyDataStore.favoriteCurrenciesKey)
        }
    }
}
