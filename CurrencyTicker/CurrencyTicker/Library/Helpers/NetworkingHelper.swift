//
//  NetworkingHelper.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 30/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Alamofire

typealias CompletionBlock = (result: [String: AnyObject]?, error: NSError?) -> Void

class NetowrkingHelper {
    let baseURL = "http://api.fixer.io/"
    
    static let sharedInstance = NetowrkingHelper()
    
    init() {
        
    }
    
    func get(path : String, parameters : [String: AnyObject]?, completionBlock : CompletionBlock) {
        print("Get : Path : \(path)")
        
        let finalURLString = baseURL + path
        let finalURL = NSURL(string: finalURLString)!
        
        let nsurlreq = NSMutableURLRequest(URL: finalURL, cachePolicy: NSURLRequestCachePolicy.ReturnCacheDataElseLoad, timeoutInterval: 100)
        
        Alamofire.request(.GET, nsurlreq, parameters: parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .Success:
                    print("Validation Successful")
                    if let JSON = response.result.value as? [String : AnyObject] {
                        print("JSON: \(JSON)")
                        completionBlock(result: JSON, error: nil)
                    } else {
                        //JSON Parsing Error
                        let error = NSError(domain: "", code: 0, userInfo: ["" : ""])
                        completionBlock(result: nil, error: error)
                    }
                    
                case .Failure(let error):
                    print(error)
                    completionBlock(result: nil, error: error)
                }
        }
    }
}
