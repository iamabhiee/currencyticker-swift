//
//  EncodingHelper.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 18/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Foundation

enum EncodingStructError: ErrorType {
    case InvalidSize
}

func encode<T>(var value: T) -> NSData {
    return withUnsafePointer(&value) { p in
        NSData(bytes: p, length: sizeofValue(value))
    }
}

func decode<T>(data: NSData) throws -> T {
    guard data.length == sizeof(T) else {
        throw EncodingStructError.InvalidSize
    }
    
    let pointer = UnsafeMutablePointer<T>.alloc(1)
    data.getBytes(pointer, length: data.length)
    
    return pointer.move()
}

enum Result<T> {
    case Success(T)
    case Failure
}