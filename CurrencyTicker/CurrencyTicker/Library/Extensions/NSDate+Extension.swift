//
//  NSDate+Extension.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 30/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Foundation

extension NSDate {
    
    var stringValue : String {
        get {
            let formatter = NSDateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter.stringFromDate(self);
        }
    }
    
    var shortStringValue : String {
        get {
            let formatter = NSDateFormatter()
            formatter.dateFormat = "MM-dd"
            return formatter.stringFromDate(self);
        }
    }
    
    class func dateFromString(dateString: String) -> NSDate! {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.dateFromString(dateString)
    }
    
    func dateAfterAddingDays(days : Double) -> NSDate {
        return self.dateByAddingTimeInterval((days * 60 * 60 * 24))
    }
    
    
}