//
//  UIColor+Extension.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 26/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import UIKit

extension UIColor {
    class func ct_redColor() -> UIColor {
        return UIColor.init(red: 198.0/255.0, green: 40.0/255.0, blue: 40.0/255.0, alpha: 1.0)
    }
    
    class func ct_greenColor() -> UIColor {
        return UIColor.init(red: 46.0/255.0, green: 125.0/255.0, blue: 50.0/255.0, alpha: 1.0)
    }
    
    class func ct_blueColor() -> UIColor {
        return UIColor.init(red: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)
    }
}