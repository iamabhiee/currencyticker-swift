//
//  UIViewController+Extension.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 30/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import UIKit
import MBProgressHUD

extension UIViewController {
    func showProgressHud() {
        let hud = MBProgressHUD.showHUDAddedTo(view, animated: true)
        view.bringSubviewToFront(hud)
    }
    
    func hideProgressHud() {
        MBProgressHUD.hideAllHUDsForView(view, animated: true)
    }
    
    func handleError(error : NSError?) {
        if let errorValue = error {
            showAlert("Error!", message: errorValue.localizedDescription)
        }
    }
    
    func showAlert(title : String, message : String) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .Default, handler: nil)
        controller.addAction(okAction)
        
        presentViewController(controller, animated: true, completion: nil)
    }
}
