//
//  PlaceholderView.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 31/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import UIKit

protocol PlaceholderViewDelegate: class {
    func didTapPlaceholderActionButton()
}

class PlaceholderView: UIView {
    
    @IBOutlet var placeholderImageView : UIImageView!
    @IBOutlet var placeholderLabel : UILabel!
    @IBOutlet var actionButton : UIButton!
    
    weak var delegate : PlaceholderViewDelegate?

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> PlaceholderView {
        return UINib(nibName: "PlaceholderView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! PlaceholderView
    }
    
    func configure(title : String, buttonTitle : String = "", imageName : String = "") {
        self.placeholderLabel.text = title
        
        if !buttonTitle.isEmpty {
            actionButton.setTitle(buttonTitle, forState: .Normal)
        } else {
            actionButton.hidden = true
        }
        
        if !imageName.isEmpty {
            placeholderImageView.image = UIImage(named: imageName)
        }
    }
    
    @IBAction func didTapActionButton(sender: UIButton? = nil) {
        delegate?.didTapPlaceholderActionButton()
    }
}
